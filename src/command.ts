import { ActionType } from "../serve/interfaces";

enum KeyType {
    RECENT = 187,
    HOME = 3,
    BACK = 4,
}

/**
 * 自定义右键命令
 * @property title 展示名称
 * @property execute  返回false 不关闭菜单，true 关闭
 */
export interface CommandItem {
    title: string;
    execute: () => Promise<boolean>;
}
export const defaultCommands: CommandItem[] = [
    {
        title: 'Home',
        cmd: KeyType.HOME
    },
    {
        title: 'Back',
        cmd: KeyType.BACK
    },
    {
        title: 'Recent',
        cmd: KeyType.RECENT
    }
].map(m => {
    return {
        title: m.title,
        execute: async () => {
            sender(`${ActionType.COMMAND}: input keyevent ${m.cmd}`);
            return true
        }    
    }
});

const menu_style: Partial<CSSStyleDeclaration> = {
    boxSizing: 'border-box',
    padding: '20px 0 40px',
    margin: '0',
    font: 'normal 20px/48px "Microsoft Yahei"',
    background: '#d2d2d2',
    position: 'absolute',
    zIndex: '-1',
    opacity: '0',
    transition: 'opacity .5s ease',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
};

let sender: WebSocket['send']
const menu_bg = document.createElement('div');
menu_bg.style.cssText = ``

const btn_close = document.createElement('a');
btn_close.innerHTML = '&times;'
btn_close.style.cssText = `
    position: absolute;
    right: 12px;
    top: 12px;
    width: 40px;
    line-height: 32px;
    text-align: center;
    cursor: pointer;
    font-size: 32px;
`;
menu_bg.appendChild(btn_close);

const btn_reset = document.createElement('a');
btn_reset.innerHTML = '设备重连'
btn_reset.style.cssText = `
    position: absolute;
    right: 12px;
    bottom: 12px;
    width: 72px;
    line-height: 32px;
    text-align: right;
    cursor: pointer;
    font-size: 16px;
`;
menu_bg.appendChild(btn_reset);


const menu = document.createElement('ul');
menu.style.cssText = `
    width: 120px;
    cursor: pointer;
    text-align: left;
    list-style: none;
`
menu_bg.appendChild(menu)

/**
 * 
 * @param param0 target: 
 */
export const showCommand = ({ target }: { target: HTMLElement }) => {
    const { left, top, width, height } = target.getBoundingClientRect()

    Object.assign(menu_bg.style, menu_style, {
        left: left + 'px',
        top: top + 'px',
        width: width + 'px',
        height: height + 'px',
        zIndex: '1',
    })
    
    document.body.appendChild(menu_bg);
    menu_bg.style.opacity = '0.9'
}

export const hideCommand = (e?: MouseEvent) => {
    Object.assign(menu_bg.style, {
        zIndex: '-1',
        opacity: '0',
    })
}

btn_close.addEventListener('click', hideCommand)
btn_reset.addEventListener('click', function (e) {
    sender(`${ActionType.RECONNECT}: `)
    hideCommand(e)
})
/**
 * 
 * @param _sender ws 连接 send 方法
 * @param commands 自定义菜单命令
 */
export const setSender = (_sender: WebSocket['send'],  commands = defaultCommands) => {
    sender = _sender
    menu.innerHTML = ''
    commands.map(m => {
        const item = document.createElement('li');
        item.innerHTML = `<a>${m.title}</a>`
        menu.appendChild(item)
        item.addEventListener('click', async function () {
            if (await m.execute()) {
                hideCommand()
            }
        })
    });
};