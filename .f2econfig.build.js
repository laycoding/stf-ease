// @ts-check

const { join } = require('path')

/**
 * @type {import('f2e-server').F2EConfig}
 */
const config = {
    buildFilter: (p) => /^(index|src|favicon|static)/.test(p),
    middlewares: [
        { middleware: 'esbuild' },
    ],
    output: join(__dirname, './stf-ease-server')
}
module.exports = config