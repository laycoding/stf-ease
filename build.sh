rm -rf ./stf-ease-server
npm run tsc
npm run build

cd ./stf-ease-server

# copy files
mkdir ./screencaps
cp -rf ../apks ./
cp -rf ../lib ./
cp ../.npmrc ./
cp ../.f2econfig.prod.js ./.f2econfig.js

# npm prepare
npm init -y
npm i @devicefarmer/minicap-prebuilt @devicefarmer/minitouch-prebuilt minitouch-prebuilt-support10
npm i ws f2e-serve f2e-server tcp-port-used gm

# write start
echo 'require("f2e-server")({})' > start.js
echo 'node ./start.js > server.log 2>&1 & echo $! > server.pid' > start.sh
echo 'cat ./server.pid | xargs kill -9' > stop.sh

cd ..
tar -zcvf ./stf-ease-server.tar.gz ./stf-ease-server