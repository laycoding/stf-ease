// @ts-check

/**
 * @type { import('f2e-middle-esbuild').BuildOptions }
 */
let config = {
    watches: [/\.tsx?$/],
    sourcemap: 'external',
    entryPoints: ['src/index.ts'],
    outfile: 'static/bundle.js',
    target: 'chrome70',
    bundle: true,
    format: 'esm',
    loader: {
        '.ts': 'ts'
    },
    tsconfig: './tsconfig.json',
};

module.exports = config