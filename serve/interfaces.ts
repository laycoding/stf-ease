
export enum ActionType {
    /** 服务端发送错误信息 */
    ERROR = '0',
    /** 服务端发送banner信息 */
    BANNER = '1',
    /** 服务端发送device信息 */
    DEVICE = '2',
    /** 服务端发送touch基础信息 */
    TOUCH_META = '3',

    /** 客户端发送adb指令 */
    COMMAND = '103',
    /** 客户端发送minitouch命令 */
    TOUCH = '104',
    /** 客户端发送断开minicap命令 */
    RECONNECT = '105',
}
export interface Banner {
    version: number;
    length: number;
    pid: number;
    realWidth: number;
    realHeight: number;
    virtualWidth: number;
    virtualHeight: number;
    orientation: number;
    quirks: number;
}
/**
 * 旋转角度
 */
export enum DeviceRotation {
    R0, R90, R180, R270
}
export interface Device {
    /** device id */
    id: string
    /** cpu 架构 */
    abi: string
    /** android sdk 版本 */
    sdk: number
    /** 品牌 */
    brand: string
    /** 型号 */
    model: string
    /** 处理器 */
    board: string
    size: {
        physical: string
        override: string
    }
    rotation?: DeviceRotation
    /** 标记更新日期 */
    create_date?: string
    /** 是否为不支持minicap的魔鬼机型 */
    is_devil?: boolean
    /** 运行时size */
    runtime_size?: string
    status?: 'device' | 'offline'
}
export interface DeviceConfig {
    support_rotate?: boolean
    /**
     * 输出截图高度
     * @default 360
     */
    screen_width?: number
    /**
     * 是否自动安装需要的apk
     * @default false
     */
    install_pkg?: boolean
    /**
     * 设置最大帧数 默认25
     */
    rate?: number
    /**
     * 不使用minicap
     */
    use_javacap?: boolean
}
export interface TouchMeta {
    /** 多点触控支持最多点数 */
    max_contacts?: number
    /** 支持最大x位置 */
    max_x?: number
    /** 支持最大y位置 */
    max_y?: number
    /** 支持最大压力值 */
    max_pressure: number
}

export interface STFEaseConfig extends DeviceConfig {
    /**
     * adb命令完整路径
     * @default "adb"
     * */
    adb?: string
    /**
     * 整个服务端口
     * @default 7100
     * */
    port?: number
    /** 
     * tcp映射端口起始值, 自动判断是否被使用, 如果已经使用则+1
     * @default 7400 
     * */
    port_tcp_start?: number

}
