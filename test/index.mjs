// @ts-check

import { STFEase } from '../lib/index.js'

const stf = new STFEase({
    adb: 'adb.exe'
})

const devices = stf.devices().map(stf.getDevice)
console.log(devices)
stf.connect_device({
    device: devices[0],
    onReady: function () {
        setTimeout(() => {
            process.exit(0)
        }, 3000);
    }
});