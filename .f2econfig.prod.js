// @ts-check
const { STFEase } = require('./lib')
const port = Number(process.env['PORT']) || 7100
const ease = new STFEase({
    /**
     * windows 环境下可以配置环境变量启动 ADB_SHELL=adb.exe
     */
    adb: process.env['ADB_SHELL'] || 'adb',
    /**
     * 要和f2e-server的端口一致
     */
    port,
    /**
     * adb forward 起始端口，会自动检查是否占用，已占用则 +1
     */
    port_tcp_start: Number(process.env['PORT_FORWARD_BEGIN']) || 7400,
    /**
     * 默认展示宽度
     */
    screen_width: Number(process.env['SCREEN_WIDTH']) || 360,
    /**
     * 是否默认安装需要的apk文件
     */
    install_pkg: process.env['INSTALL_PKG'] === 'true',
    /**
     * 帧率限制
     */
    rate: Number(process.env['RATE']) || 30,
})
/**
 * @type {import('f2e-server').F2EConfig}
 */
const config = {
    port,
    gzip: true,
    livereload: process.env['DEV'] === 'true',
    buildFilter: (p) => /^(index|static)/.test(p),
    middlewares: [
        ease.create_route,
    ],
    // @ts-ignore
    onServerCreate: ease.create_server
}
module.exports = config