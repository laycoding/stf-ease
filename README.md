# STF-EASE
STF 简化版
- 仅保留Android可视化和操作基础功能
- 支持多设备。
- 合并对外服务端口为1个

## 预览
- 简单预览  
![简单预览](./images/preview.png)
- 批量操作  
![批量操作](./images/muti.gif)

## 环境依赖
1. adb
2. node.js(v14+) + npm(yarn)
3. ImageMagick / GraphicsMagick

## 服务端使用
- npm install / yarn install
- npm run tsc
- npm run dev `DEBUG_LEVEL=error PORT=7100 RATE=30 INSTALL_PKG=false npm run dev`
    | 参数名  |  描述  | 类型/选项 |  默认值  |
    | ---- | ---- | ---- | ---- |
    | **DEBUG_LEVEL** | 日志级别 | "info"/"error" | "error" |
    | **PORT** | HTTP(WS)服务端口设置 | `number` | 7100 |
    | **RATE** | 最高帧率 | `number` | 30 |
    | **INSTALL_PKG** | 是否自动安装依赖的APK | `boolean` | false |
    | *SCREEN_WIDTH* | 默认展示宽度 | `number` | 360 |
    | *PORT_FORWARD_BEGIN* | forward 起始端口自动检查占用 | `number` | 7400
    | *ADB_SHELL* | adb命令，windows下可配置 *ADB_SHELL=adb.exe* | `string` | "adb"
    > 所有环境变量均为可选项 

- 访问 [`http://localhost:7100`](http://localhost:7100)

## 发布版
- sh build.sh 获得 `stf-ease-server.tar.gz`
- 目标机器解压 ./stf-ease-server.tar.gz
- cd stf-ease-server
- sh start.sh  `DEBUG_LEVEL=error PORT=7100 RATE=30 INSTALL_PKG=false sh start.sh`
- sh stop.sh 关闭


## 客户端使用

`npm i stf-ease`

**鼓励自定义UI** 参见 [index.html](./index.html)  

```js
import Ease from "stf-ease"

Ease({
    ws: new WebSocket(`ws://127.0.0.1:7100/abcdefg`),
    canvas: document.querySelector('canvas'),
    onDevice: device => {
        console.log(device)
    },
    onTouchMeta: function (meta) {
        if (meta) {
            console.log('minitouch初始化成功')
        } else {
            console.log('minitouch初始化失败！')
        }
    },
    onClose: function () {
        console.log('close')
    }
})
```